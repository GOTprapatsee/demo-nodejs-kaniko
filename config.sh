AUTH=$(echo -n "${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}" | base64)
cat << EOF > config.json
{
    "auths": {
        "${CI_REGISTRY}": {
            "auth": "${AUTH}"
        }
    }
}
EOF